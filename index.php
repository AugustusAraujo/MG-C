<link rel="stylesheet" href="style.css">
<!doctype html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script defer src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</head>

<body>

<?php
    require_once("config.php");

    define("SERVER_URL",$_SERVER['HTTP_HOST']);
    /*
if ($_GET['id']) {
    if (isset($_GET["id"])) {
        switch ($id) {
            case "home":
                include("paginas/login.php");
                break;
            case "":
                include("paginas/login.php");
                break;
            case "area":
                include("paginas/area.php");
                break;
            case "vl":
                include("scripts/valida_login.php");
                break;
            case "va":
                include("scripts/valida_adicionar.php");
                break;
            case "sair":
                include("scripts/logout.php");
                break;
        }
    } else {
        header('location: ?id=home');
    }
}*/


    $app = new \Slim\Slim();

    $app->get("/", function () {
        require_once("pages" . DIRECTORY_SEPARATOR . "home.php");
    });

    $app->get("/login", function () {
        
        require_once("pages" . DIRECTORY_SEPARATOR . "login.php");
        
    });
    

    // ERRORS
    $app->notFound(function () use ($app) {
        echo file_get_contents("templates/404.html");
    });

    $app->error(function (\Exception $e) use ($app) {
        $app->render('error.php');
    });

    $app->run();
    ?>

</body>

</html>