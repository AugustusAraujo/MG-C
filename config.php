<?php
//Require do autoload

require_once("vendor" . DIRECTORY_SEPARATOR . "autoload.php");

// Require das classes

spl_autoload_register(function ($className) {
    $filename = "class" . DIRECTORY_SEPARATOR . $className . ".php";
    if (file_exists($filename)) {
        require_once($filename);
    }
});

// CONFIG RAINTPL

use Rain\Tpl;

$config = array(
    "tpl_dir"       => "templates" . DIRECTORY_SEPARATOR . "tpl",
    "cache_dir"     => "templates" . DIRECTORY_SEPARATOR . "cache"
);
Tpl::configure($config);
